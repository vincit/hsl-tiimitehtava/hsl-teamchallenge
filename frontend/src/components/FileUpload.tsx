import React, { useState } from "react";

import { onUploadFiles } from "../api/services";

export const FileUpload = () => {
  const [files, setFiles] = useState<FileList>();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [success, setSuccess] = useState<undefined | boolean>(undefined);

  const onChange = (fileList: FileList) => {
    setFiles(fileList);
    setSuccess(undefined);
  };

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setIsSubmitting(true);
    setSuccess(undefined);

    const formData = new FormData();

    if (files) {
      Array.from(files).forEach((file) => {
        formData.append("myFile", file);
      });
    }
    try {
      await onUploadFiles(formData);
      setSuccess(true);
    } catch (err) {
      setSuccess(false);
    }
    setIsSubmitting(false);
  };

  return (
    <div>
      <h1>Upload timetables xml</h1>
      <form onChange={(e: any) => onChange(e.target.files)} onSubmit={onSubmit}>
        <div>
          <input type="file" name="file" id="files" accept=".xml" />
        </div>
        <button type="submit" disabled={!files?.length}>
          Upload
        </button>
        {isSubmitting && <p>Uploading...</p>}
        {success && <p>Upload success</p>}
        {success === false && <p>Something went wrong</p>}
      </form>
    </div>
  );
};
