#!/usr/bin/env bash

set -euxo pipefail

# This script attempts to mimic the core functionality of .gitlab-ci.yml

# 1. build-deps-image / fetch-deps

IMAGE_NAME=hsl-teamchallenge
TAG_NAME=deps_latest

BUILD_DOCKERFILE=./backend/Dockerfile_deps
DOCKER_CONTEXT=./backend

docker build --cache-from $IMAGE_NAME:$TAG_NAME \
             --tag $IMAGE_NAME:$TAG_NAME \
             -f $BUILD_DOCKERFILE \
             -t $IMAGE_NAME \
             $DOCKER_CONTEXT
# 2. build-jars / build

COMPILE_DOCKERFILE=./backend/Dockerfile_build
COMPILE_IMAGE=hsl-teamchallenge-built
COMPILE_TAG_NAME=latest

# Note that we eagerly cache the previous build result, unlike gitlab-ci.yml

docker build --build-arg IMAGE_SOURCE=$IMAGE_NAME \
             --build-arg IMAGE_TAG=$TAG_NAME \
             --cache-from $COMPILE_IMAGE:$COMPILE_TAG_NAME \
             --tag $COMPILE_IMAGE:$COMPILE_TAG_NAME \
             -f $COMPILE_DOCKERFILE \
             -t $COMPILE_IMAGE \
             $DOCKER_CONTEXT

# Extract .JAR
JAR_SRC=/usr/src/myapp/target/hsl-teamchallenge-0.0.1-SNAPSHOT.jar
JAR_DEST=./backend/hsl-teamchallenge-0.0.1-SNAPSHOT.jar
docker run --rm --entrypoint cat $COMPILE_IMAGE:$COMPILE_TAG_NAME $JAR_SRC > $JAR_DEST

# To build and run the release ui image
RELEASE_DOCKERFILE=./backend/Dockerfile_release
RELEASE_IMAGE=hsl-teamchallenge-release
RELEASE_TAG_NAME=latest
docker build -f $RELEASE_DOCKERFILE -t $RELEASE_IMAGE:$RELEASE_TAG_NAME $DOCKER_CONTEXT
docker run $RELEASE_IMAGE:$RELEASE_TAG_NAME
