package fi.vincit.hslteamchallenge.feature.health_check.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.time.Instant;

@Value.Immutable
@JsonSerialize(as = ImmutableHealthCheckResponse.class)
public interface HealthCheckResponse {
    Instant now();

    boolean dbConnectionAlive();

    static HealthCheckResponse build(final boolean dbIsAlive) {
        return ImmutableHealthCheckResponse.builder()
                                           .now(Instant.now())
                                           .dbConnectionAlive(dbIsAlive)
                                           .build();
    }
}
