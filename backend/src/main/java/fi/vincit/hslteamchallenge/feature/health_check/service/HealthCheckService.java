package fi.vincit.hslteamchallenge.feature.health_check.service;

import fi.vincit.hslteamchallenge.feature.health_check.dto.HealthCheckResponse;
import org.springframework.stereotype.Service;

@Service
public class HealthCheckService implements IHealthCheckService {
    @Override
    public HealthCheckResponse check() {
        return HealthCheckResponse.build(true);
    }
}
