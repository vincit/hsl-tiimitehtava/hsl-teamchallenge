# Technology choices

**React** is used as the de-facto modern UI platform for the web.

**Java** is used as a mature language and ecosystem for enterprise requirements.

**PostgreSQL** is used as the relational database that checks data integrity and provides a transactional platform with strong ACID compliance.
PostgreSQL is provided as a managed service on Azure which HSL is using.
PostgreSQL is also available on several other cloud vendors which reduces the risk of cloud jail.

**Kubernetes** is used as the orchestration platform.
Kubernetes is provided as a managed platform on Azure which HSL is using.
Also many other cloud vendors provide Kubernetes which reduces the risk of cloud jail.

**Docker** is used as the de-facto standard for providing reproducible containers.
