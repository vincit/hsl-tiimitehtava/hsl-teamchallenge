import axios from "axios";

const baseURL = "localhost:TODO";

export const ApiClient = axios.create({
  timeout: 5000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  baseURL,
});
