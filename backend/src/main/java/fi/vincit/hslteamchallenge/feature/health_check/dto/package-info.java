@NonNullApi
@NonNullFields
package fi.vincit.hslteamchallenge.feature.health_check.dto;

import org.springframework.lang.NonNullApi;
import org.springframework.lang.NonNullFields;
