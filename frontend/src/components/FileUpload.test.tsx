import React from "react";
import { render } from "@testing-library/react";
import { FileUpload } from "./FileUpload";

test("Renders file upload ui", () => {
  const { container } = render(<FileUpload />);
  expect(container.firstChild).toMatchInlineSnapshot(`
    <div>
      <h1>
        Upload timetables xml
      </h1>
      <form>
        <div>
          <input
            accept=".xml"
            id="files"
            name="file"
            type="file"
          />
        </div>
        <button
          disabled=""
          type="submit"
        >
          Upload
        </button>
      </form>
    </div>
  `);
});
