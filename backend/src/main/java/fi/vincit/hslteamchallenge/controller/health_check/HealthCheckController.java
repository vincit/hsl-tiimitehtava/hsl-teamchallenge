package fi.vincit.hslteamchallenge.controller.health_check;

import fi.vincit.hslteamchallenge.feature.health_check.dto.HealthCheckResponse;
import fi.vincit.hslteamchallenge.feature.health_check.service.IHealthCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/healthcheck")
public class HealthCheckController {

    private final IHealthCheckService healthCheckService;

    @Autowired
    public HealthCheckController(final IHealthCheckService healthCheckService) {
        this.healthCheckService = healthCheckService;
    }

    @GetMapping(value = "/",
                produces = "application/json")
    public ResponseEntity<HealthCheckResponse> healthCheck() {
        final HealthCheckResponse resp = healthCheckService.check();
        return resp.dbConnectionAlive() ?
                ResponseEntity.ok(resp) :
                ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(resp);
    }
}
