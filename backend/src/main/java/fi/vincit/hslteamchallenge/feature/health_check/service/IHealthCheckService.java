package fi.vincit.hslteamchallenge.feature.health_check.service;

import fi.vincit.hslteamchallenge.feature.health_check.dto.HealthCheckResponse;

public interface IHealthCheckService {
    HealthCheckResponse check();
}
