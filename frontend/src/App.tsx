import React from 'react';

import { FileUpload } from './components/FileUpload';
import { Timetable } from './components/Timetable';
import './App.css';

function App() {
  return (
    <div className="App">
      <FileUpload />
      <Timetable />
    </div>
  );
}

export default App;
