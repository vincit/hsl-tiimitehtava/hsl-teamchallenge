import React from "react";
import "./Timetable.css";

export const Timetable = () => {
  const data = [
    {
      id: "asdf",
      trainNumber: 1,
      station: "Tre",
      arrivalTime: "11:00",
      departureTime: "11:05",
    },
    {
      id: "hjkl",
      trainNumber: 2,
      station: "Tre",
      arrivalTime: "12:00",
      departureTime: "12:05",
    },
  ];

  return (
    <div id="timetable" >
      <h2>Example data</h2>
      <table >
        <thead>
          <tr>
            <th>Train number</th>
            <th>Station</th>
            <th>Arrival time</th>
            <th>Departure time</th>
          </tr>
        </thead>
        <tbody>
          {data.map(
            ({ id, trainNumber, station, arrivalTime, departureTime }) => (
              <tr key={id}>
                <td>{trainNumber}</td>
                <td>{station}</td>
                <td>{arrivalTime}</td>
                <td>{departureTime}</td>
              </tr>
            )
          )}
        </tbody>
      </table>
    </div>
  );
};
