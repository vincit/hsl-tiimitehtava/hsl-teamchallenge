# Deployment to Azure

## Preliminaries

Install Azure CLI:
https://docs.microsoft.com/en-us/cli/azure/install-azure-cli

Install Helm:
https://helm.sh/docs/intro/install/

## Only once

(based on https://medium.com/microsoftazure/deploy-a-full-stack-web-app-to-azure-kubernetes-service-with-docker-fb3d23f7294b)

1. Create ingress namespace:

```
# Create a K8s namespace for the ingress resources
kubectl create namespace ingress-basic
# Add the ingress-nginx repository
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
# Use Helm to deploy an NGINX ingress controller
helm install nginx-ingress ingress-nginx/ingress-nginx \
     --namespace ingress-basic \
     --set controller.replicaCount=2 \
     --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
     --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux
```

## How To

1. Log in to Azure subscription:
   `az login`

2. Log in to AKS Cluster:
   `az aks get-credentials --resource-group hsl-staging-rg --name HSLStagingCluster`

3. Deploy ingress with: (if changed)
   `kubectl apply -f deploy/hsl-ingress.yaml --namespace ingress-basic`

4. Deploy backend with:
   `kubectl apply -f deploy/hsl-backend.yaml --namespace ingress-basic`

5. Deploy frontend with:
   `kubectl apply -f deploy/hsl-frontend.yaml --namespace ingress-basic`

## Checking/validation:

Running services (external IP found from here):
`kubectl --namespace ingress-basic get services -o wide -w nginx-ingress-ingress-nginx-controller`

Running pods:
`kubectl get pods --all-namespaces=true`

Check Kubernetes dashboard (after logging in with `az aks get-credentials ...`):

1. `az aks browse --resource-group hsl-staging-rg --name HSLStagingCluster`
2. Choose Kubeconfig
3. Find your own Kubeconfig file from `~/.kube/config` (In MacOS, use CMD+Shift+. to show hidden files)
