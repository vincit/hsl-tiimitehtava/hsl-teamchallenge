# Architecture

React UI <-> Java backend <-> PostgreSQL

Java-backend interprets the XML and inserts it into the database.

## Deployment

The resources are deployed to Azure cloud. The frontend and backend are served from Docker containers and are hosted in AKS.

The code (both frontend, backend and the deployment scripts) are hosted in Gitlab. CI/CD is run through Gitlab CI.

Scripts and instructions for the Azure deployment can be found from `deploy/README.md`.

### CI/CD steps:

#### Building:

1. Create docker image for preloading external dependencies (node_modules, Maven libraries). Useful for caching.
2. Build the frontend and backend artifacts (bundle and jar) within a docker image on top of the dependency image.
3. Run unit tests for both frontend and backend in a docker image.
4. Host the artifacts and serve them in the release docker image as frontend and backend.
5. Push the release docker images to Azure ACR. Tag them with the git hash for traceability.

#### Deployment:

1. Login to Azure with a machine user (`ci-service-principal`) that has access to ACR and AKS.
2. Update the Kubernetes cluster backend, frontend and ingress. They should load the latest docker images and host them.

#### Checks:

1. After successful deployment, run e2e tests.
