# HSL team challenge

Load Juna-aikataulut IVU XML data into PostgreSQL.

The architecture is described in [Architecture](./architecture.md).

The technologies used are described in [Technology choices](./technology.md).
