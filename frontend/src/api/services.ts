import { ApiClient } from "./ApiClient";

export const onUploadFiles = async (data: FormData) => {
  try {
    const res = await ApiClient.post(`/api/xml/import`, data);
    return res.data;
  } catch (err) {
    return Promise.reject(err);
  }
};
