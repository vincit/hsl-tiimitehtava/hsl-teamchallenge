package fi.vincit.hslteamchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HslTeamchallengeApplication {

    public static void main(final String[] args) {
        SpringApplication.run(HslTeamchallengeApplication.class, args);
    }

}
